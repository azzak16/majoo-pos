<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\DB;

class Transaction extends Model
{
    use HasFactory, SoftDeletes;

    protected $guarded = [];

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function transactionItems()
    {
        return $this->hasMany(TransactionItem::class);
    }

    public static function getEdit($id)
    {
        $data = Transaction::select('transactions.id',
                                'transactions.total_price',
                                'transactions.status',
                                'transactions.code',
                                'users.name',
                                'customers.alamat',
                                'customers.telp',
                                'products.harga',
                                'products.nama',
                                DB::raw('(select count(transaction_items.product_id) as qty)')
                                )
            ->leftJoin('users', 'transactions.user_id', '=', 'users.id')
            ->leftJoin('customers', 'users.customer_id', '=', 'customers.id')
            ->leftJoin('transaction_items', 'transaction_items.transaction_id', '=', 'transactions.id')
            ->leftJoin('products', 'transaction_items.product_id', '=', 'products.id')
            ->where('transactions.id', $id)
            ->groupBy('transaction_items.product_id',
                    'transactions.id',
                    'transactions.total_price',
                    'transactions.status',
                    'transactions.code',
                    'users.name',
                    'customers.alamat',
                    'customers.telp',
                    'products.harga',
                    'products.nama'
                )
            ->get();

            return $data;
    }
}
