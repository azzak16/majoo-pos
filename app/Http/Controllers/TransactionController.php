<?php

namespace App\Http\Controllers;

use App\Models\Cart;
use App\Models\Product;
use App\Models\Transaction;
use App\Models\TransactionItem;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Yajra\DataTables\Facades\DataTables;
use Illuminate\Support\Str;

class TransactionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public $data = [
        'title' => 'Transaksi',
    ];

    public function index()
    {
        if (request()->ajax()) {


            $data = Transaction::with('user');
            if (Auth::user()->role != 'ADMIN') {
                $data->where('user_id', Auth::user()->id);
            }
            $query = $data->get();

            return  DataTables::of($query)
                ->addIndexColumn()
                ->addColumn('customer', function($item){
                    return $item->user->name;
                })
                ->addColumn('action', function($item){
                    $edit = '';
                    if (Auth::user()->role == 'ADMIN') {
                        $edit = '<a class="btn btn-primary btn-sm" href="'.route('transaction.edit', $item->id).'">
                                    Edit
                                </a>';
                    }
                    return $edit.'
                        <a class="btn btn-warning btn-sm" href="'.route('transaction.show', $item->id).'">
                            Detail
                        </a>
                    ';
                })
                ->editColumn('total_price', function($item){
                    return 'Rp. '.number_format($item->total_price);
                })
                ->make();
        }

        return view('transaction.index', $this->data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        DB::beginTransaction();
        $data = Transaction::create([
            'user_id' => Auth::user()->id,
            'status' => 'WAITING',
            'code' => Str::random(3).'/'.Carbon::now()->format('Y-m-d')
        ]);
        if (!$data) {
            DB::rollback();
            return response()->json([
                'status'    => false,
                'message'   => $data
            ], 400);
        }

        $harga = 0;

        if ($request->product_id) {
            foreach ($request->product_id as $key => $value) {
                $product = Product::find($value);

                $harga += $product->harga;

                $titem = TransactionItem::create([
                    'transaction_id' => $data->id,
                    'product_id' => $value
                ]);

                if (!$titem) {
                    DB::rollback();
                    return response()->json([
                        'status'    => false,
                        'message'   => $titem
                    ], 400);
                }

                $cart = Cart::where('product_id', $value)->where('user_id', Auth::user()->id)->first();
                $cart->delete();

            }
        }

        $data->total_price = $harga;
        $data->save();

        DB::commit();
        return response()->json([
            'status'    => true,
            'results'   => route('transaction.index')
        ], 200);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $this->data['data'] = Transaction::getEdit($id);

        return view('transaction.show', $this->data);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $this->data['data'] = Transaction::getEdit($id);

        return view('transaction.edit', $this->data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        DB::beginTransaction();
        $product = Transaction::find($id);
        $product->update($request->all());

        if (!$product) {
            DB::rollBack();
            return response()->json([
                'status'    => false,
                'message'   => $product
            ], 400);
        }

        DB::commit();
        return response()->json([
            'status'    => true,
            'results'   => route('transaction.index')
        ], 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

}
