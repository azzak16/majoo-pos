<?php

namespace App\Http\Controllers;

use App\Models\Cart;
use App\Models\Product;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class WelcomeController extends Controller
{
    public function index()
    {
        $product = Product::with('category')->get();

        return view('welcome', compact('product'));
    }

    public function chart(Request $request)
    {
        if (!Auth::check()) {
            return response()->json([
                'status'    => false,
                'message'   => 'Kamu Harus Login Dulu'
            ], 400);
        }

        DB::beginTransaction();
        $cart = Cart::create([
            'product_id' => $request->product_id,
            'user_id' => Auth::user()->id
        ]);

        if (!$cart) {
            DB::rollback();
            return response()->json([
                'status'    => false,
                'message'   => $cart
            ], 400);
        }

        DB::commit();
        return response()->json([
            'status'    => true,
            'message'   => 'Produk berhasil ditambahkan'
        ], 200);
    }
}
