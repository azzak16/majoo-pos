<?php

namespace App\Http\Controllers;

use App\Http\Requests\ProductRequest;
use App\Models\Product;
use App\Models\ProductCategory;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Yajra\DataTables\Facades\DataTables;
use Illuminate\Support\Str;

class ProductController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public $data = [
        'title' => 'Produk',
    ];

    public function index()
    {

        if (request()->ajax()) {

            $query = Product::with('category')
            ->get();

            return  DataTables::of($query)
                ->addIndexColumn()
                ->addColumn('action', function($item){
                    return '
                        <a class="btn btn-primary btn-sm" href="'.route('product.edit', $item->id).'">
                            Edit
                        </a>
                        <a class="btn btn-warning btn-sm" href="'.route('product.show', $item->id).'">
                            Detail
                        </a>
                        <a class="btn btn-danger btn-sm delete" href="#" data-url="'.route('product.destroy', $item->id).'">
                            Delete
                        </a>
                    ';
                })
                ->editColumn('harga', function($item){
                    return 'Rp. '.number_format($item->harga);
                })
                ->make();
        }

        return view('product.index', $this->data);
    }

    public function categoryselect(Request $request)
    {
        $start = $request->page ? $request->page - 1 : 0;
        $length = $request->limit;
        $name = strtoupper($request->name);
        //Select Pagination
        $query = ProductCategory::whereRaw("upper(name) like '%$name%'");

        $clone = clone $query;

        $query->offset($start);
        $query->limit($length);
        $items = $query->get();

        $data = [];
        foreach ($items as $item) {
            $item->no = ++$start;
            $data[] = $item;
        }
        return response()->json([
            'total' => $clone->count(),
            'rows' => $data
        ], 200);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('product.create', $this->data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(ProductRequest $request)
    {
        if ($request->validator && $request->validator->fails()) {
            return response()->json([
                'status'    => false,
                'message'   => $request->validator->getMessageBag()->first()
            ], 400);
        }

        DB::beginTransaction();
        $product = Product::create([
            'nama' => $request->nama,
            'harga' => $request->harga,
            'deskripsi' => $request->deskripsi,
            'slug' => Str::slug($request->nama),
            'product_category_id' => $request->category_id,
            'image' => ''
        ]);

        if($request->file('image')){
            $src = 'assets/product/' . $product->id;
            if (!file_exists($src)) {
                mkdir($src, 0777, true);
            }

            $image = $request->file('image');
            $image->move($src, $product->slug . '.' . $image->getClientOriginalExtension());
            $filename = $src . '/' . $product->slug . '.' . $image->getClientOriginalExtension();

            $product->image = $filename;
            $product->save();

        }

        if (!$product) {
            DB::rollBack();
            return response()->json([
                'status'    => false,
                'message'   => $product
            ], 400);
        }

        DB::commit();
        return response()->json([
            'status'    => true,
            'results'   => route('product.index')
        ], 200);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $this->data['data'] = Product::with('category')->findOrFail($id);

        return view('product.show', $this->data);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $this->data['data'] = Product::with('category')->findOrFail($id);

        return view('product.edit', $this->data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(ProductRequest $request, $id)
    {
        if ($request->validator && $request->validator->fails()) {
            return response()->json([
                'status'    => false,
                'message'   => $request->validator->getMessageBag()->first()
            ], 400);
        }

        DB::beginTransaction();
        $product = Product::find($id);
        $product->nama = $request->nama;
        $product->harga = $request->harga;
        $product->deskripsi = $request->deskripsi;
        $product->slug = Str::slug($request->nama);
        $product->product_category_id = $request->category_id;
        $product->save();

        if($request->file('image')){

            if(file_exists($product->image)){
                unlink($product->image);
            }

            $src = 'assets/product/' . $product->id;
            if (!file_exists($src)) {
                mkdir($src, 0777, true);
            }

            $image = $request->file('image');
            $image->move($src, $product->slug . '.' . $image->getClientOriginalExtension());
            $filename = $src . '/' . $product->slug . '.' . $image->getClientOriginalExtension();

            $product->image = $filename;
            $product->save();

        }

        if (!$product) {
            DB::rollBack();
            return response()->json([
                'status'    => false,
                'message'   => $product
            ], 400);
        }

        DB::commit();
        return response()->json([
            'status'    => true,
            'results'   => route('product.index')
        ], 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
            $product = Product::find($id);
            if(file_exists($product->image)){
                unlink($product->image);
            }
            $product->delete();
        } catch (\Illuminate\Database\QueryException $e) {
            return response()->json([
                'status'     => false,
                'message'     => 'Error delete data'
            ], 400);
        }
        return response()->json([
            'status'     => true,
            'message' => 'Success delete data'
        ], 200);
    }
}
