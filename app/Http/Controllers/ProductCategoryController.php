<?php

namespace App\Http\Controllers;

use App\Http\Requests\ProductCategoryRequest;
use App\Models\ProductCategory;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Yajra\DataTables\Facades\DataTables;

class ProductCategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public $data = [
        'title' => 'Kategori Produk',
    ];

    public function index()
    {
        if (request()->ajax()) {

            $query = ProductCategory::get();


            return  DataTables::of($query)
                ->addIndexColumn()
                ->addColumn('action', function($item){
                    return '
                        <a class="btn btn-primary btn-sm" href="'.route('productcategory.edit', $item->id).'">
                            Edit
                        </a>
                        <a class="btn btn-warning btn-sm" href="'.route('productcategory.show', $item->id).'">
                            Detail
                        </a>
                        <a class="btn btn-danger btn-sm delete" href="#" data-url="'.route('productcategory.destroy', $item->id).'">
                            Delete
                        </a>
                    ';
                })
                ->make();
        }

        return view('productcategory.index', $this->data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('productcategory.create', $this->data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(ProductCategoryRequest $request)
    {
        if ($request->validator && $request->validator->fails()) {
            return response()->json([
                'status'    => false,
                'message'   => $request->validator->getMessageBag()->first()
            ], 400);
        }

        DB::beginTransaction();
        $product = ProductCategory::create($request->all());

        if (!$product) {
            DB::rollBack();
            return response()->json([
                'status'    => false,
                'message'   => $product
            ], 400);
        }

        DB::commit();
        return response()->json([
            'status'    => true,
            'results'   => route('productcategory.index')
        ], 200);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $this->data['data'] = ProductCategory::findOrFail($id);

        return view('productcategory.show', $this->data);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $this->data['data'] = ProductCategory::findOrFail($id);

        return view('productcategory.edit', $this->data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        if ($request->validator && $request->validator->fails()) {
            return response()->json([
                'status'    => false,
                'message'   => $request->validator->getMessageBag()->first()
            ], 400);
        }

        DB::beginTransaction();
        $product = ProductCategory::find($id);
        $product->update($request->all());

        if (!$product) {
            DB::rollBack();
            return response()->json([
                'status'    => false,
                'message'   => $product
            ], 400);
        }

        DB::commit();
        return response()->json([
            'status'    => true,
            'results'   => route('productcategory.index')
        ], 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
            $product = ProductCategory::find($id);
            $product->delete();
        } catch (\Illuminate\Database\QueryException $e) {
            return response()->json([
                'status'     => false,
                'message'     => 'Error delete data'
            ], 400);
        }
        return response()->json([
            'status'     => true,
            'message' => 'Success delete data'
        ], 200);
    }
}
