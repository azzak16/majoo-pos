<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Auth;

class ProductRequest extends FormRequest
{
    public $validator   = null;

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return Auth::check();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        switch ($this->method()) {
            case 'PUT':
                return [
                    'nama' =>'required|unique:products,nama,' . $this->all()['id'],
                    'deskripsi' =>'required',
                    'harga' =>'required|integer',
                    'category_id' =>'required',
                    'image' => 'image'
                ];
                break;

            default:
                return [
                    'nama' =>'required|unique:products,nama',
                    'deskripsi' =>'required',
                    'harga' =>'required|integer',
                    'category_id' =>'required',
                    'image' => 'image'
                ];
                break;
        }
    }

    public function failedValidation(\Illuminate\Contracts\Validation\Validator $validator)
    {
        $this->validator    = $validator;
    }
}
