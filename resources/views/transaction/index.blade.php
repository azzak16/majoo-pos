@extends('layouts.app')

@section('stylesheets')
<link href="{{asset('component/dataTables/css/datatables.min.css')}}" rel="stylesheet">
@endsection

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">
                    {{ $title }}
                    {{-- <a href="{{ route('product.create') }}" class="btn btn-success btn-sm float-right">+ Tambah {{ $title }}</a> --}}
                </div>

                <div class="card-body">
                    <div class="table-responsive">
                        <table class="table table-striped table-bordered" id="table-product">
                            <thead>
                                <tr>
                                    <th>No</th>
                                    <th>Kode Transaksi</th>
                                    <th>Nama Pelanggan</th>
                                    <th>status</th>
                                    <th>aksi</th>
                                </tr>
                            </thead>
                            <tbody>

                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@push('scripts')
    <script src="{{asset('component/dataTables/js/datatables.min.js')}}"></script>
    <script>
        $(document).ready(function(){

            dataTable = $('#table-product').DataTable({
            stateSave:true,
            processing:true,
            serverSide:true,
            info:false,
            lengthChange:true,
            responsive:true,
            ajax: {
                url: '{!! url()->current() !!}',
                type: "GET",
                data: function(data){
                },
            },
            columnDefs: [
            {
                render: function(data, type, row) {
					switch (data) {
                        case 'WAITING':
                            return `<span class="badge badge-warning">Waiting</span>`;
                            break;
                        case 'PROSES':
                            return `<span class="badge badge-primary">Proses</span>`;
                            break;
                        case 'REJECT':
                            return `<span class="badge badge-danger">Reject</span>`;
                            break;
                        case 'FINISH':
                            return `<span class="badge badge-success">Finish</span>`;
                            break;

                        default:
                            break;
                    }
				}, targets:[3]
            },
        ],
            columns: [
                { data: "DT_RowIndex" },
                { data: "code" },
                { data: "customer" },
                { data: "status", class:'text-center' },
                { data: "action", orderable: false, searchable: false, class: 'text-center' },
            ]
        });

        $(document).on('click','.delete',function(){
            var url = $(this).data('url');
            bootbox.confirm({
                buttons: {
                    confirm: {
                        label: '<i class="fa fa-check"></i>',
                        className: 'btn btn-danger'
                    },
                    cancel: {
                        label: '<i class="fa fa-undo"></i>',
                        className: 'btn btn-secondary'
                    },
                },
                title:'Hapus Produk Kategori?',
                message:'Data yang dihapus tdak bisa dikembalikan',
                callback: function(result) {
                        if(result) {
                            var data = {
                                _token: "{{ csrf_token() }}"
                            };
                            $.ajax({
                                url: url,
                                dataType: 'json',
                                data:data,
                                type:'DELETE',
                                beforeSend:function(){
                                    $('.overlay').removeClass('hidden');
                                }
                            }).done(function(response){
                                if(response.status){
                                    $('.overlay').addClass('hidden');
                                    $.gritter.add({
                                        title: 'Success!',
                                        text: response.message,
                                        class_name: 'gritter-success',
                                        time: 1000,
                                    });
                                    dataTable.ajax.reload( null, false );
                                }
                                else{
                                    $.gritter.add({
                                        title: 'Warning!',
                                        text: response.message,
                                        class_name: 'gritter-warning',
                                        time: 1000,
                                    });
                                }
                            }).fail(function(response){
                                var response = response.responseJSON;
                                $('.overlay').addClass('hidden');
                                $.gritter.add({
                                    title: 'Error!',
                                    text: response.message,
                                    class_name: 'gritter-error',
                                    time: 1000,
                                });
                            })
                        }
                }
            });
        })
    });

    </script>
@endpush
