@extends('layouts.app')

@section('stylesheets')
<link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.13/css/select2.min.css" rel="stylesheet" />
<link href="{{asset('component/bootstrap-fileinput/css/fileinput.min.css')}}" rel="stylesheet">
<link href="{{asset('component/bootstrap-fileinput/themes/explorer-fas/theme.min.css')}}" rel="stylesheet">
<style>
    .select2-container--default{
        width: inherit !important;
    }
    .select2-container--default .select2-selection--single {
        background-color: #F3F7FF !important;
        height: 34px !important;
    }
    .select2-container--default .select2-selection--single .select2-selection__placeholder{
        color: #c4c4c4;
    }
    .select2-container{
        width: 100% !important;
    }
    .select2-selection{
        height: 2.7em !important;
        padding-top: 0.3em;
    }
</style>
@endsection

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Edit {{ $title }}</div>

                <div class="card-body">

                    <form id="form" action="{{ route('transaction.update', $data[0]->id) }}" method="POST">
                        @csrf
                        @method('put')
                        <input type="hidden" name="id" class="form-control" value="{{ $data[0]->id }}">
                        <div class="col-sm-12">
                            <div class="row">
                                <p class="col-md-2"> Pelanggan </p>
                                <p class="col-md-1"> : </p>
                                <p class="col-md-6">{{ $data[0]->name }}</p>
                            </div>
                        </div>
                        <div class="col-sm-12">
                            <div class="row">
                                <p class="col-md-2"> Kode transaksi </p>
                                <p class="col-md-1"> : </p>
                                <p class="col-md-6">{{ $data[0]->code }}</p>
                            </div>
                        </div>
                        <div class="col-sm-12">
                            <div class="row">
                                <p class="col-md-2"> Telp </p>
                                <p class="col-md-1"> : </p>
                                <p class="col-md-6">{{ $data[0]->telp ?? '-' }}</p>
                            </div>
                        </div>
                        <div class="col-sm-12">
                            <div class="row">
                                <p class="col-md-2"> Alamat </p>
                                <p class="col-md-1"> : </p>
                                <p class="col-md-6">{{ $data[0]->alamat ?? '-' }}</p>
                            </div>
                        </div>
                        <div class="col-sm-12">
                            <div class="row">
                                <p class="col-md-2"> Status </p>
                                <p class="col-md-1">: </p>
                                <p class="col-md-6">{{ $data[0]->status }}</p>
                            </div>
                        </div>
                        <table class="table">
                            <thead>
                                <tr>
                                    <th>Produk</th>
                                    <th>Harga</th>
                                    <th>Qty</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($data as $item)
                                    <tr>
                                        <td>{{ $item->nama }}</td>
                                        <td>Rp. {{ number_format($item->harga) }}</td>
                                        <td>{{ $item->qty }}</td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                        <div class="col-sm-12 ">
                            <div class="row">
                                <p class="col-md-2"> Total </p>
                                <p class="col-md-1">: </p>
                                <p class="col-md-6">Rp. {{ number_format($data[0]->total_price) }}</p>
                            </div>
                        </div>
                        <div class="float-right">
                            @if ($data[0]->status == 'PROSES')
                                <a href="#" data-status="FINISH" class="btn btn-success submit">Finish</a>
                            @elseif($data[0]->status == 'WAITING')
                                <a href="#" data-status="PROSES" class="btn btn-warning submit">Proses</a>
                                <a href="#" data-status="REJECT" class="btn btn-danger submit">Reject</a>
                            @endif
                        </div>
                    </form>

                </div>
                <div class="overlay d-none">
                    <i class="fa fa-2x fa-sync-alt fa-spin"></i>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@push('scripts')
<script src="{{asset('component/validate/jquery.validate.min.js')}}"></script>
<script src="https://cdn.ckeditor.com/ckeditor5/31.0.0/classic/ckeditor.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.13/js/select2.min.js"></script>
<script src="{{asset('component/bootstrap-fileinput/js/fileinput.min.js')}}"></script>
<script src="{{asset('component/bootstrap-fileinput/themes/explorer-fas/theme.min.js')}}"></script>
    <script>

        $(document).ready(function(){

            $(document).on('click','.submit',function(){
                var data = new FormData($('#form')[0]);
                var status = $(this).data('status');
                data.append('status', status);

                $.ajax({
                    url:$('#form').attr('action'),
                    method:'post',
                    data: data,
                    processData: false,
                    contentType: false,
                    dataType: 'json',
                    beforeSend:function(){
                    $('.overlay').removeClass('d-none');
                    }
                }).done(function(response){
                    $('.overlay').addClass('d-none');
                    if(response.status){
                    document.location = response.results;
                    }
                    else{
                    $.gritter.add({
                        title: 'Warning!',
                        text: response.message,
                        class_name: 'gritter-warning',
                        time: 1000,
                    });
                    }
                    return;
                }).fail(function(response){
                    $('.overlay').addClass('d-none');
                    var response = response.responseJSON;
                    $.gritter.add({
                    title: 'Error!',
                    text: response.message,
                    class_name: 'gritter-error',
                    time: 1000,
                    });
                })
            })

        });

    </script>
@endpush
