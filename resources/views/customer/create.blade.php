@extends('layouts.app')

@section('stylesheets')
<link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.13/css/select2.min.css" rel="stylesheet" />
<link href="{{asset('component/bootstrap-fileinput/css/fileinput.min.css')}}" rel="stylesheet">
<link href="{{asset('component/bootstrap-fileinput/themes/explorer-fas/theme.min.css')}}" rel="stylesheet">
<style>
    .select2-container--default{
        width: inherit !important;
    }
    .select2-container--default .select2-selection--single {
        background-color: #F3F7FF !important;
        height: 34px !important;
    }
    .select2-container--default .select2-selection--single .select2-selection__placeholder{
        color: #c4c4c4;
    }
    .select2-container{
        width: 100% !important;
    }
    .select2-selection{
        height: 2.7em !important;
        padding-top: 0.3em;
    }
</style>
@endsection

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Tambah {{ $title }}</div>

                <div class="card-body">

                    <form id="form" action="{{ route('product.store') }}" method="POST">
                        @csrf
                        <div class="col-sm-12">
                            <div class="form-group">
                                <label>Nama Produk <b class="text-danger">*</b></label>
                                <input type="text" name="nama" class="form-control" placeholder="Nama Produk">
                            </div>
                        </div>
                        <div class="col-sm-12">
                            <div class="form-group">
                                <label>Deskripsi <b class="text-danger">*</b></label>
                                <textarea style="height: 120px;" class="form-control" id="deskripsi" name="deskripsi"
                                    placeholder="Deskripsi"></textarea>
                            </div>
                        </div>
                        <div class="col-sm-12">
                            <div class="form-group">
                                <label>Harga Produk <b class="text-danger">*</b></label>
                                <input type="number" name="harga" class="form-control" placeholder="Harga Produk">
                            </div>
                        </div>
                        <div class="col-sm-12">
                            <div class="form-group">
                                <label>Produk Kategori <b class="text-danger">*</b></label>
                                <select class="form-control" id="category_id" name="category_id">
                                    <option value="">asd</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-sm-12">
                            <div class="form-group row">
                                <label for="image" class="col-sm-2 col-form-label">Gambar</label>
                                <div class="col-sm-12">
                                    <input type="file" class="form-control" name="image" id="image" accept="image/*" />
                                </div>
                            </div>
                        </div>
                        <div class="float-right">
                            <button type="submit" class="btn btn-success">Simpan</button>
                        </div>
                    </form>
                </div>
                <div class="overlay d-none">
                    <i class="fa fa-2x fa-sync-alt fa-spin"></i>
                </div>
            </div>
        </div>
    </div>
</div>
                
@endsection

@push('scripts')
<script src="{{asset('component/validate/jquery.validate.min.js')}}"></script>
<script src="https://cdn.ckeditor.com/ckeditor5/31.0.0/classic/ckeditor.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.13/js/select2.min.js"></script>
<script src="{{asset('component/bootstrap-fileinput/js/fileinput.min.js')}}"></script>
<script src="{{asset('component/bootstrap-fileinput/themes/explorer-fas/theme.min.js')}}"></script>
    <script>

        $(document).ready(function(){
            // ckeditor
            ClassicEditor
            .create( document.querySelector( '#deskripsi' ) )
            .catch( error => {
                console.error( error );
            } );

            // select cateogry
            $( "#category_id" ).select2({
                ajax: {
                    url: "{{url('product/categoryselect')}}",
                    type:'GET',
                    dataType: 'json',
                    data: function (params) {
                        var query = {
                            name: params.term,
                            page: params.page || 1,
                            limit: 30
                        }
                    return query;
                    },
                    processResults: function (data, params) {
                        params.page = params.page || 1;

                        return {
                            results: data.rows,
                            pagination: {
                                more: (params.page * 30) < data.total
                            }
                        };
                    },
                    cache: true
                },
                placeholder: 'Produk Kategori',
                templateResult: formatRepo,
                templateSelection: formatRepoSelection
            });

            $("#image").fileinput({
                browseClass: "btn btn-primary",
                showRemove: false,
                showUpload: true,
                allowedFileExtensions: ["png", "jpg", "jpeg"],
                dropZoneEnabled: false,
                initialPreviewFileType: 'image',
                theme: 'explorer-fas'
            });

            $("#form").validate({
                errorElement: 'div',
                errorClass: 'invalid-feedback',
                focusInvalid: false,
                highlight: function (e) {
                    $(e).closest('.form-group').removeClass('has-success').addClass('was-validated has-error');
                },

                success: function (e) {
                    $(e).closest('.form-group').removeClass('has-error').addClass('has-success');
                    $(e).remove();
                },
                errorPlacement: function (error, element) {
                    if(element.is(':file')) {
                    error.insertAfter(element.parent().parent().parent());
                    }else
                    if(element.parent('.input-group').length) {
                    error.insertAfter(element.parent());
                    }
                    else
                    if (element.attr('type') == 'checkbox') {
                        error.insertAfter(element.parent());
                    }
                    else{
                        error.insertAfter(element);
                    }
                    },
                    submitHandler: function() {
                    $.ajax({
                        url:$('#form').attr('action'),
                        method:'post',
                        data: new FormData($('#form')[0]),
                        processData: false,
                        contentType: false,
                        dataType: 'json',
                        beforeSend:function(){
                          $('.overlay').removeClass('d-none');
                        }
                    }).done(function(response){
                        $('.overlay').addClass('d-none');
                        if(response.status){
                            document.location = response.results;
                        }
                        else{
                        $.gritter.add({
                            title: 'Warning!',
                            text: response.message,
                            class_name: 'gritter-warning',
                            time: 1000,
                        });
                        }
                        return;
                    }).fail(function(response){
                        $('.overlay').addClass('d-none');
                        var response = response.responseJSON;
                        $.gritter.add({
                            title: 'Error!',
                            text: response.message,
                            class_name: 'gritter-error',
                            time: 1000,
                        });
                    })
                }
            });


        });
        function formatRepo (data) {
            if (data.loading) {
                return data.text;
            }

            var markup = data.name;

            return markup;
        }

        function formatRepoSelection (repo) {
            return repo.name || repo.text;
        }

    </script>
@endpush
