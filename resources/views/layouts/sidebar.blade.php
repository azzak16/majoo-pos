<aside class="main-sidebar sidebar-light-{{config('configs.app_theme')}} elevation-4">
    <!-- Brand Logo -->
    <a href="{{ url('/') }}" class="brand-link navbar-{{config('configs.app_theme')}}">
        <img src="{{asset(config('configs.app_icon'))}}" alt="{{config('configs.app_name')}}"
            class="brand-image img-circle elevation-3">
        <span class="brand-text text-dark">{{config('configs.app_name')}}</span>
    </a>

    <!-- Sidebar -->
    <div class="sidebar">
        <!-- Sidebar user panel (optional) -->
        <div class="profile-info">
            <figure class="user-cover-image"></figure>
            <div class="user-info">
                <img src="{{asset('adminlte/images/user2-160x160.jpg')}}" class="img-circle elevation-2"
                    alt="img">
                <h6>{{ Auth::user()->name }}</h6>
            </div>
        </div>
        <nav class="mt-2">
            <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu"
                data-accordion="false">
                <li>Dashboard</li>
            </ul>
        </nav>
        <!-- /.sidebar-menu -->
    </div>
    <!-- /.sidebar -->
</aside>
