<div class="collapse navbar-collapse" id="navbarSupportedContent">
                    <!-- Left Side Of Navbar -->
                    <ul class="navbar-nav me-auto">
                        @auth
                            <li class="nav-item">
                                <a class="nav-link {{ request()->is('home') ? 'active' : ''}}" href="{{ url('/home') }}">Dashboard</a>
                            </li>
                            @if (Auth::user()->role == 'ADMIN')
                                <li class="nav-item">
                                    <a class="nav-link {{ request()->is('product') ? 'active' : ''}}" href="{{ route('product.index') }}">Product</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link {{ request()->is('productcategory') ? 'active' : ''}}" href="{{ route('productcategory.index') }}">Product Category</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link {{ request()->is('customer') ? 'active' : ''}}" href="{{ route('customer.index') }}">Customer</a>
                                </li>
                            @endif
                            <li class="nav-item">
                                <a class="nav-link {{ request()->is('cart') ? 'active' : ''}}" href="{{ route('cart.index') }}">Cart</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link {{ request()->is('transaction') ? 'active' : ''}}" href="{{ route('transaction.index') }}">Transaction</a>
                            </li>
                        @endauth
                    </ul>

                    <!-- Right Side Of Navbar -->
                    <ul class="navbar-nav ms-auto">
                        <!-- Authentication Links -->
                        @guest
                            @if (Route::has('login'))
                                <li class="nav-item">
                                    <a class="nav-link" href="{{ route('login') }}">{{ __('Login') }}</a>
                                </li>
                            @endif

                            @if (Route::has('register'))
                                <li class="nav-item">
                                    <a class="nav-link" href="{{ route('register') }}">{{ __('Register') }}</a>
                                </li>
                            @endif
                        @else
                            <li class="nav-item dropdown">
                                <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-bs-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                                    {{ Auth::user()->name }}
                                </a>

                                <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                                    <a class="dropdown-item" href="{{ route('logout') }}"
                                       onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                        {{ __('Logout') }}
                                    </a>

                                    <form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
                                        @csrf
                                    </form>
                                </div>
                            </li>
                        @endguest
                    </ul>
                </div>
