@extends('layouts.app')

@section('stylesheets')
<link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.13/css/select2.min.css" rel="stylesheet" />
<link href="{{asset('component/bootstrap-fileinput/css/fileinput.min.css')}}" rel="stylesheet">
<link href="{{asset('component/bootstrap-fileinput/themes/explorer-fas/theme.min.css')}}" rel="stylesheet">
<style>
    .select2-container--default{
        width: inherit !important;
    }
    .select2-container--default .select2-selection--single {
        background-color: #F3F7FF !important;
        height: 34px !important;
    }
    .select2-container--default .select2-selection--single .select2-selection__placeholder{
        color: #c4c4c4;
    }
    .select2-container{
        width: 100% !important;
    }
    .select2-selection{
        height: 2.7em !important;
        padding-top: 0.3em;
    }
</style>
@endsection

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Edit {{ $title }}</div>

                <div class="card-body">

                    <form id="form" action="{{ route('productcategory.update', $data->id) }}" method="POST">
                        @csrf
                        @method('put')
                        <input type="hidden" name="id" class="form-control" value="{{ $data->id }}">
                        <div class="col-sm-12">
                            <div class="form-group">
                                <label>Nama Kategori Produk <b class="text-danger">*</b></label>
                                <input type="text" name="name" class="form-control" value="{{ $data->name }}" placeholder="Nama Kategori Produk">
                            </div>
                        </div>
                        <div class="float-right">
                            <button type="submit" class="btn btn-success">Simpan</button>
                        </div>
                    </form>

                </div>
                <div class="overlay d-none">
                    <i class="fa fa-2x fa-sync-alt fa-spin"></i>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@push('scripts')
<script src="{{asset('component/validate/jquery.validate.min.js')}}"></script>
<script src="https://cdn.ckeditor.com/ckeditor5/31.0.0/classic/ckeditor.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.13/js/select2.min.js"></script>
<script src="{{asset('component/bootstrap-fileinput/js/fileinput.min.js')}}"></script>
<script src="{{asset('component/bootstrap-fileinput/themes/explorer-fas/theme.min.js')}}"></script>
    <script>

        $(document).ready(function(){

            $("#form").validate({
                errorElement: 'div',
                errorClass: 'invalid-feedback',
                focusInvalid: false,
                highlight: function (e) {
                    $(e).closest('.form-group').removeClass('has-success').addClass('was-validated has-error');
                },

                success: function (e) {
                    $(e).closest('.form-group').removeClass('has-error').addClass('has-success');
                    $(e).remove();
                },
                errorPlacement: function (error, element) {
                    if(element.is(':file')) {
                    error.insertAfter(element.parent().parent().parent());
                    }else
                    if(element.parent('.input-group').length) {
                    error.insertAfter(element.parent());
                    }
                    else
                    if (element.attr('type') == 'checkbox') {
                        error.insertAfter(element.parent());
                    }
                    else{
                        error.insertAfter(element);
                    }
                    },
                    submitHandler: function() {
                    $.ajax({
                        url:$('#form').attr('action'),
                        method:'post',
                        data: new FormData($('#form')[0]),
                        processData: false,
                        contentType: false,
                        dataType: 'json',
                        beforeSend:function(){
                        $('.overlay').removeClass('d-none');
                        }
                    }).done(function(response){
                        $('.overlay').addClass('d-none');
                        if(response.status){
                        document.location = response.results;
                        }
                        else{
                        $.gritter.add({
                            title: 'Warning!',
                            text: response.message,
                            class_name: 'gritter-warning',
                            time: 1000,
                        });
                        }
                        return;
                    }).fail(function(response){
                        $('.overlay').addClass('d-none');
                        var response = response.responseJSON;
                        $.gritter.add({
                        title: 'Error!',
                        text: response.message,
                        class_name: 'gritter-error',
                        time: 1000,
                        });
                    })
                }
            });


        });

    </script>
@endpush
