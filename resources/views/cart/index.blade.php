@extends('layouts.app')

@section('stylesheets')
<link href="{{asset('component/dataTables/css/datatables.min.css')}}" rel="stylesheet">
@endsection

@section('content')
<div class="container">
    <form id="form">
    <div class="row justify-content-center">
            @csrf
            <div class="col-md-8">
                @forelse ($data as $item)
                <div class="card">
                    <div class="row">
                        <div class=" col-md-2 m-2">
                            <img src="{{ asset($item->product->image) }}" alt="" height="100px" width="100px" style="object-fit: cover">
                            <input type="hidden" id="product_id" name="product_id[]" value="{{ $item->product_id }}">
                        </div>
                        <div class="col-md-4 mt-4">
                            <b>{{ $item->product->nama }}</b>
                            <br>
                            <p >Rp. {{ number_format($item->product->harga) }}</p>
                        </div>
                        <div class="col-md-5 mt-4">
                            <a class="float-right remove" data-id="{{ $item->id }}" href="javascript:void(0)"><i class="fas fa-trash"></i></a>
                        </div>
                    </div>
                </div>
                @empty
                <div class="card m-5 justify-content-center text-center" style="height: 50px">
                    Cart Kosong
                </div>
                @endforelse

                <div class="float-right">
                    @if (count($data) > 0)
                    <a href="#" class="btn btn-success process">Process</a>
                    @endif
                </div>
            </div>
        </div>
    </form>
</div>
@endsection

@push('scripts')
    <script src="{{asset('component/dataTables/js/datatables.min.js')}}"></script>
    <script>
        $(document).ready(function(){

        $(document).on('click','.process',function(){
            var data = new FormData($('#form')[0]);
            $.ajax({
                url: "{{ route('transaction.store') }}",
                method:'post',
                data: data,
                processData: false,
                contentType: false,
                dataType: 'json',
                beforeSend:function(){
                    $('.overlay').removeClass('d-none');
                }
            }).done(function(response){
                $('.overlay').addClass('d-none');
                if(response.status){
                    document.location = response.results;
                }
                else{
                $.gritter.add({
                    title: 'Warning!',
                    text: response.message,
                    class_name: 'gritter-warning',
                    time: 1000,
                });
                }
                return;
            }).fail(function(response){
                $('.overlay').addClass('d-none');
                var response = response.responseJSON;
                $.gritter.add({
                    title: 'Error!',
                    text: response.message,
                    class_name: 'gritter-error',
                    time: 1000,
                });
            })
        });

        $(document).on('click','.remove',function(){
            var ini = $(this)
            var id = ini.data('id');
            bootbox.confirm({
                buttons: {
                    confirm: {
                        label: '<i class="fa fa-check"></i>',
                        className: 'btn btn-danger'
                    },
                    cancel: {
                        label: '<i class="fa fa-undo"></i>',
                        className: 'btn btn-secondary'
                    },
                },
                title:'Hapus Product?',
                message:'Data yang dihapus tdak bisa dikembalikan',
                callback: function(result) {
                        if(result) {
                            var data = {
                                _token: "{{ csrf_token() }}"
                            };
                            $.ajax({
                                url:"{{ 'cart/' }}" + id,
                                dataType: 'json',
                                data:data,
                                type:'DELETE',
                                beforeSend:function(){
                                    $('.overlay').removeClass('hidden');
                                }
                            }).done(function(response){
                                if(response.status){
                                    $('.overlay').addClass('hidden');
                                    $.gritter.add({
                                        title: 'Success!',
                                        text: response.message,
                                        class_name: 'gritter-success',
                                        time: 1000,
                                    });
                                    ini.parents('.card').remove();

                                }
                                else{
                                    $.gritter.add({
                                        title: 'Warning!',
                                        text: response.message,
                                        class_name: 'gritter-warning',
                                        time: 1000,
                                    });
                                }
                            }).fail(function(response){
                                var response = response.responseJSON;
                                $('.overlay').addClass('hidden');
                                $.gritter.add({
                                    title: 'Error!',
                                    text: response.message,
                                    class_name: 'gritter-error',
                                    time: 1000,
                                });
                            })
                        }
                }
            });
        })
    });

    </script>
@endpush
