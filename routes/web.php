<?php

use App\Http\Controllers\CartController;
use App\Http\Controllers\CustomerController;
use App\Http\Controllers\ProductCategoryController;
use App\Http\Controllers\ProductController;
use App\Http\Controllers\TransactionController;
use App\Http\Controllers\WelcomeController;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [WelcomeController::class, 'index']);
Route::post('/chart', [WelcomeController::class, 'chart']);

Auth::routes();

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');

Route::middleware('auth')->group( function () {
    Route::get('product/categoryselect', [ProductController::class, 'categoryselect']);
    Route::resource('product', ProductController::class);

    Route::resource('productcategory', ProductCategoryController::class);
    Route::resource('customer', CustomerController::class);

    Route::resource('cart', CartController::class);
    Route::resource('transaction', TransactionController::class);
});

